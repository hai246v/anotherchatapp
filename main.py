#!/usr/bin/python3

import tornado.ioloop
import tornado.web
import tornado.websocket
import os

chat_socket_handlers = []

def write_chat_message(message):
    for e in chat_socket_handlers:
        e.write_message(message)
        print(message)

class ChatSocketHandlder(tornado.websocket.WebSocketHandler):
    def open(self):
        chat_socket_handlers.append(self)
        print("WebSocket opened")

    def on_message(self, message):
        write_chat_message(message)

    def on_close(self):
        chat_socket_handlers.remove(self)
        print("WebSocket closed")

def make_app():
    return tornado.web.Application([
        (r"/chat_socket",ChatSocketHandlder),
        (r"/(.*)",tornado.web.StaticFileHandler,{"path": os.getcwd(),"default_filename":"index.html"})
    ])

if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()